// GENERATED CODE - DO NOT MODIFY BY HAND

part of "iso_country.dart";

sealed class IsoCountries {
	/// Aruba
	/// 
	/// alpha3: ABW
	/// 
	/// alpha2: AW
	/// 
	/// numeric: 533
	static const ABW = IsoCountry._(Countries.abw);

	/// The Islamic Republic of Afghanistan
	/// 
	/// alpha3: AFG
	/// 
	/// alpha2: AF
	/// 
	/// numeric: 004
	static const AFG = IsoCountry._(Countries.afg);

	/// The Republic of Angola
	/// 
	/// alpha3: AGO
	/// 
	/// alpha2: AO
	/// 
	/// numeric: 024
	static const AGO = IsoCountry._(Countries.ago);

	/// Anguilla
	/// 
	/// alpha3: AIA
	/// 
	/// alpha2: AI
	/// 
	/// numeric: 660
	static const AIA = IsoCountry._(Countries.aia);

	/// Åland
	/// 
	/// alpha3: ALA
	/// 
	/// alpha2: AX
	/// 
	/// numeric: 248
	static const ALA = IsoCountry._(Countries.ala);

	/// The Republic of Albania
	/// 
	/// alpha3: ALB
	/// 
	/// alpha2: AL
	/// 
	/// numeric: 008
	static const ALB = IsoCountry._(Countries.alb);

	/// The Principality of Andorra
	/// 
	/// alpha3: AND
	/// 
	/// alpha2: AD
	/// 
	/// numeric: 020
	static const AND = IsoCountry._(Countries.and);

	/// The United Arab Emirates
	/// 
	/// alpha3: ARE
	/// 
	/// alpha2: AE
	/// 
	/// numeric: 784
	static const ARE = IsoCountry._(Countries.are);

	/// The Argentine Republic
	/// 
	/// alpha3: ARG
	/// 
	/// alpha2: AR
	/// 
	/// numeric: 032
	static const ARG = IsoCountry._(Countries.arg);

	/// The Republic of Armenia
	/// 
	/// alpha3: ARM
	/// 
	/// alpha2: AM
	/// 
	/// numeric: 051
	static const ARM = IsoCountry._(Countries.arm);

	/// The Territory of American Samoa
	/// 
	/// alpha3: ASM
	/// 
	/// alpha2: AS
	/// 
	/// numeric: 016
	static const ASM = IsoCountry._(Countries.asm);

	/// Antarctica
	/// 
	/// alpha3: ATA
	/// 
	/// alpha2: AQ
	/// 
	/// numeric: 010
	static const ATA = IsoCountry._(Countries.ata);

	/// The French Southern and Antarctic Lands
	/// 
	/// alpha3: ATF
	/// 
	/// alpha2: TF
	/// 
	/// numeric: 260
	static const ATF = IsoCountry._(Countries.atf);

	/// Antigua and Barbuda
	/// 
	/// alpha3: ATG
	/// 
	/// alpha2: AG
	/// 
	/// numeric: 028
	static const ATG = IsoCountry._(Countries.atg);

	/// The Commonwealth of Australia
	/// 
	/// alpha3: AUS
	/// 
	/// alpha2: AU
	/// 
	/// numeric: 036
	static const AUS = IsoCountry._(Countries.aus);

	/// The Republic of Austria
	/// 
	/// alpha3: AUT
	/// 
	/// alpha2: AT
	/// 
	/// numeric: 040
	static const AUT = IsoCountry._(Countries.aut);

	/// The Republic of Azerbaijan
	/// 
	/// alpha3: AZE
	/// 
	/// alpha2: AZ
	/// 
	/// numeric: 031
	static const AZE = IsoCountry._(Countries.aze);

	/// The Republic of Burundi
	/// 
	/// alpha3: BDI
	/// 
	/// alpha2: BI
	/// 
	/// numeric: 108
	static const BDI = IsoCountry._(Countries.bdi);

	/// The Kingdom of Belgium
	/// 
	/// alpha3: BEL
	/// 
	/// alpha2: BE
	/// 
	/// numeric: 056
	static const BEL = IsoCountry._(Countries.bel);

	/// The Republic of Benin
	/// 
	/// alpha3: BEN
	/// 
	/// alpha2: BJ
	/// 
	/// numeric: 204
	static const BEN = IsoCountry._(Countries.ben);

	/// Bonaire, Sint Eustatius and Saba
	/// 
	/// alpha3: BES
	/// 
	/// alpha2: BQ
	/// 
	/// numeric: 535
	static const BES = IsoCountry._(Countries.bes);

	/// Burkina Faso
	/// 
	/// alpha3: BFA
	/// 
	/// alpha2: BF
	/// 
	/// numeric: 854
	static const BFA = IsoCountry._(Countries.bfa);

	/// The People's Republic of Bangladesh
	/// 
	/// alpha3: BGD
	/// 
	/// alpha2: BD
	/// 
	/// numeric: 050
	static const BGD = IsoCountry._(Countries.bgd);

	/// The Republic of Bulgaria
	/// 
	/// alpha3: BGR
	/// 
	/// alpha2: BG
	/// 
	/// numeric: 100
	static const BGR = IsoCountry._(Countries.bgr);

	/// The Kingdom of Bahrain
	/// 
	/// alpha3: BHR
	/// 
	/// alpha2: BH
	/// 
	/// numeric: 048
	static const BHR = IsoCountry._(Countries.bhr);

	/// The Commonwealth of The Bahamas
	/// 
	/// alpha3: BHS
	/// 
	/// alpha2: BS
	/// 
	/// numeric: 044
	static const BHS = IsoCountry._(Countries.bhs);

	/// Bosnia and Herzegovina
	/// 
	/// alpha3: BIH
	/// 
	/// alpha2: BA
	/// 
	/// numeric: 070
	static const BIH = IsoCountry._(Countries.bih);

	/// The Collectivity of Saint-Barthélemy
	/// 
	/// alpha3: BLM
	/// 
	/// alpha2: BL
	/// 
	/// numeric: 652
	static const BLM = IsoCountry._(Countries.blm);

	/// The Republic of Belarus
	/// 
	/// alpha3: BLR
	/// 
	/// alpha2: BY
	/// 
	/// numeric: 112
	static const BLR = IsoCountry._(Countries.blr);

	/// Belize
	/// 
	/// alpha3: BLZ
	/// 
	/// alpha2: BZ
	/// 
	/// numeric: 084
	static const BLZ = IsoCountry._(Countries.blz);

	/// Bermuda
	/// 
	/// alpha3: BMU
	/// 
	/// alpha2: BM
	/// 
	/// numeric: 060
	static const BMU = IsoCountry._(Countries.bmu);

	/// The Plurinational State of Bolivia
	/// 
	/// alpha3: BOL
	/// 
	/// alpha2: BO
	/// 
	/// numeric: 068
	static const BOL = IsoCountry._(Countries.bol);

	/// The Federative Republic of Brazil
	/// 
	/// alpha3: BRA
	/// 
	/// alpha2: BR
	/// 
	/// numeric: 076
	static const BRA = IsoCountry._(Countries.bra);

	/// Barbados
	/// 
	/// alpha3: BRB
	/// 
	/// alpha2: BB
	/// 
	/// numeric: 052
	static const BRB = IsoCountry._(Countries.brb);

	/// The Nation of Brunei, the Abode of Peace
	/// 
	/// alpha3: BRN
	/// 
	/// alpha2: BN
	/// 
	/// numeric: 096
	static const BRN = IsoCountry._(Countries.brn);

	/// The Kingdom of Bhutan
	/// 
	/// alpha3: BTN
	/// 
	/// alpha2: BT
	/// 
	/// numeric: 064
	static const BTN = IsoCountry._(Countries.btn);

	/// Bouvet Island
	/// 
	/// alpha3: BVT
	/// 
	/// alpha2: BV
	/// 
	/// numeric: 074
	static const BVT = IsoCountry._(Countries.bvt);

	/// The Republic of Botswana
	/// 
	/// alpha3: BWA
	/// 
	/// alpha2: BW
	/// 
	/// numeric: 072
	static const BWA = IsoCountry._(Countries.bwa);

	/// The Central African Republic
	/// 
	/// alpha3: CAF
	/// 
	/// alpha2: CF
	/// 
	/// numeric: 140
	static const CAF = IsoCountry._(Countries.caf);

	/// Canada
	/// 
	/// alpha3: CAN
	/// 
	/// alpha2: CA
	/// 
	/// numeric: 124
	static const CAN = IsoCountry._(Countries.can);

	/// The Territory of Cocos (Keeling) Islands
	/// 
	/// alpha3: CCK
	/// 
	/// alpha2: CC
	/// 
	/// numeric: 166
	static const CCK = IsoCountry._(Countries.cck);

	/// The Swiss Confederation
	/// 
	/// alpha3: CHE
	/// 
	/// alpha2: CH
	/// 
	/// numeric: 756
	static const CHE = IsoCountry._(Countries.che);

	/// The Republic of Chile
	/// 
	/// alpha3: CHL
	/// 
	/// alpha2: CL
	/// 
	/// numeric: 152
	static const CHL = IsoCountry._(Countries.chl);

	/// The People's Republic of China
	/// 
	/// alpha3: CHN
	/// 
	/// alpha2: CN
	/// 
	/// numeric: 156
	static const CHN = IsoCountry._(Countries.chn);

	/// The Republic of Côte d'Ivoire
	/// 
	/// alpha3: CIV
	/// 
	/// alpha2: CI
	/// 
	/// numeric: 384
	static const CIV = IsoCountry._(Countries.civ);

	/// The Republic of Cameroon
	/// 
	/// alpha3: CMR
	/// 
	/// alpha2: CM
	/// 
	/// numeric: 120
	static const CMR = IsoCountry._(Countries.cmr);

	/// The Democratic Republic of the Congo
	/// 
	/// alpha3: COD
	/// 
	/// alpha2: CD
	/// 
	/// numeric: 180
	static const COD = IsoCountry._(Countries.cod);

	/// The Republic of the Congo
	/// 
	/// alpha3: COG
	/// 
	/// alpha2: CG
	/// 
	/// numeric: 178
	static const COG = IsoCountry._(Countries.cog);

	/// The Cook Islands
	/// 
	/// alpha3: COK
	/// 
	/// alpha2: CK
	/// 
	/// numeric: 184
	static const COK = IsoCountry._(Countries.cok);

	/// The Republic of Colombia
	/// 
	/// alpha3: COL
	/// 
	/// alpha2: CO
	/// 
	/// numeric: 170
	static const COL = IsoCountry._(Countries.col);

	/// The Union of the Comoros
	/// 
	/// alpha3: COM
	/// 
	/// alpha2: KM
	/// 
	/// numeric: 174
	static const COM = IsoCountry._(Countries.com);

	/// The Republic of Cabo Verde
	/// 
	/// alpha3: CPV
	/// 
	/// alpha2: CV
	/// 
	/// numeric: 132
	static const CPV = IsoCountry._(Countries.cpv);

	/// The Republic of Costa Rica
	/// 
	/// alpha3: CRI
	/// 
	/// alpha2: CR
	/// 
	/// numeric: 188
	static const CRI = IsoCountry._(Countries.cri);

	/// The Republic of Cuba
	/// 
	/// alpha3: CUB
	/// 
	/// alpha2: CU
	/// 
	/// numeric: 192
	static const CUB = IsoCountry._(Countries.cub);

	/// The Country of Curaçao
	/// 
	/// alpha3: CUW
	/// 
	/// alpha2: CW
	/// 
	/// numeric: 531
	static const CUW = IsoCountry._(Countries.cuw);

	/// The Territory of Christmas Island
	/// 
	/// alpha3: CXR
	/// 
	/// alpha2: CX
	/// 
	/// numeric: 162
	static const CXR = IsoCountry._(Countries.cxr);

	/// The Cayman Islands
	/// 
	/// alpha3: CYM
	/// 
	/// alpha2: KY
	/// 
	/// numeric: 136
	static const CYM = IsoCountry._(Countries.cym);

	/// The Republic of Cyprus
	/// 
	/// alpha3: CYP
	/// 
	/// alpha2: CY
	/// 
	/// numeric: 196
	static const CYP = IsoCountry._(Countries.cyp);

	/// The Czech Republic
	/// 
	/// alpha3: CZE
	/// 
	/// alpha2: CZ
	/// 
	/// numeric: 203
	static const CZE = IsoCountry._(Countries.cze);

	/// The Federal Republic of Germany
	/// 
	/// alpha3: DEU
	/// 
	/// alpha2: DE
	/// 
	/// numeric: 276
	static const DEU = IsoCountry._(Countries.deu);

	/// The Republic of Djibouti
	/// 
	/// alpha3: DJI
	/// 
	/// alpha2: DJ
	/// 
	/// numeric: 262
	static const DJI = IsoCountry._(Countries.dji);

	/// The Commonwealth of Dominica
	/// 
	/// alpha3: DMA
	/// 
	/// alpha2: DM
	/// 
	/// numeric: 212
	static const DMA = IsoCountry._(Countries.dma);

	/// The Kingdom of Denmark
	/// 
	/// alpha3: DNK
	/// 
	/// alpha2: DK
	/// 
	/// numeric: 208
	static const DNK = IsoCountry._(Countries.dnk);

	/// The Dominican Republic
	/// 
	/// alpha3: DOM
	/// 
	/// alpha2: DO
	/// 
	/// numeric: 214
	static const DOM = IsoCountry._(Countries.dom);

	/// The People's Democratic Republic of Algeria
	/// 
	/// alpha3: DZA
	/// 
	/// alpha2: DZ
	/// 
	/// numeric: 012
	static const DZA = IsoCountry._(Countries.dza);

	/// The Republic of Ecuador
	/// 
	/// alpha3: ECU
	/// 
	/// alpha2: EC
	/// 
	/// numeric: 218
	static const ECU = IsoCountry._(Countries.ecu);

	/// The Arab Republic of Egypt
	/// 
	/// alpha3: EGY
	/// 
	/// alpha2: EG
	/// 
	/// numeric: 818
	static const EGY = IsoCountry._(Countries.egy);

	/// The State of Eritrea
	/// 
	/// alpha3: ERI
	/// 
	/// alpha2: ER
	/// 
	/// numeric: 232
	static const ERI = IsoCountry._(Countries.eri);

	/// The Sahrawi Arab Democratic Republic
	/// 
	/// alpha3: ESH
	/// 
	/// alpha2: EH
	/// 
	/// numeric: 732
	static const ESH = IsoCountry._(Countries.esh);

	/// The Kingdom of Spain
	/// 
	/// alpha3: ESP
	/// 
	/// alpha2: ES
	/// 
	/// numeric: 724
	static const ESP = IsoCountry._(Countries.esp);

	/// The Republic of Estonia
	/// 
	/// alpha3: EST
	/// 
	/// alpha2: EE
	/// 
	/// numeric: 233
	static const EST = IsoCountry._(Countries.est);

	/// The Federal Democratic Republic of Ethiopia
	/// 
	/// alpha3: ETH
	/// 
	/// alpha2: ET
	/// 
	/// numeric: 231
	static const ETH = IsoCountry._(Countries.eth);

	/// The Republic of Finland
	/// 
	/// alpha3: FIN
	/// 
	/// alpha2: FI
	/// 
	/// numeric: 246
	static const FIN = IsoCountry._(Countries.fin);

	/// The Republic of Fiji
	/// 
	/// alpha3: FJI
	/// 
	/// alpha2: FJ
	/// 
	/// numeric: 242
	static const FJI = IsoCountry._(Countries.fji);

	/// The Falkland Islands
	/// 
	/// alpha3: FLK
	/// 
	/// alpha2: FK
	/// 
	/// numeric: 238
	static const FLK = IsoCountry._(Countries.flk);

	/// The French Republic
	/// 
	/// alpha3: FRA
	/// 
	/// alpha2: FR
	/// 
	/// numeric: 250
	static const FRA = IsoCountry._(Countries.fra);

	/// The Faroe Islands
	/// 
	/// alpha3: FRO
	/// 
	/// alpha2: FO
	/// 
	/// numeric: 234
	static const FRO = IsoCountry._(Countries.fro);

	/// The Federated States of Micronesia
	/// 
	/// alpha3: FSM
	/// 
	/// alpha2: FM
	/// 
	/// numeric: 583
	static const FSM = IsoCountry._(Countries.fsm);

	/// The Gabonese Republic
	/// 
	/// alpha3: GAB
	/// 
	/// alpha2: GA
	/// 
	/// numeric: 266
	static const GAB = IsoCountry._(Countries.gab);

	/// The United Kingdom of Great Britain and Northern Ireland
	/// 
	/// alpha3: GBR
	/// 
	/// alpha2: GB
	/// 
	/// numeric: 826
	static const GBR = IsoCountry._(Countries.gbr);

	/// Georgia
	/// 
	/// alpha3: GEO
	/// 
	/// alpha2: GE
	/// 
	/// numeric: 268
	static const GEO = IsoCountry._(Countries.geo);

	/// The Bailiwick of Guernsey
	/// 
	/// alpha3: GGY
	/// 
	/// alpha2: GG
	/// 
	/// numeric: 831
	static const GGY = IsoCountry._(Countries.ggy);

	/// The Republic of Ghana
	/// 
	/// alpha3: GHA
	/// 
	/// alpha2: GH
	/// 
	/// numeric: 288
	static const GHA = IsoCountry._(Countries.gha);

	/// Gibraltar
	/// 
	/// alpha3: GIB
	/// 
	/// alpha2: GI
	/// 
	/// numeric: 292
	static const GIB = IsoCountry._(Countries.gib);

	/// The Republic of Guinea
	/// 
	/// alpha3: GIN
	/// 
	/// alpha2: GN
	/// 
	/// numeric: 324
	static const GIN = IsoCountry._(Countries.gin);

	/// Guadeloupe
	/// 
	/// alpha3: GLP
	/// 
	/// alpha2: GP
	/// 
	/// numeric: 312
	static const GLP = IsoCountry._(Countries.glp);

	/// The Republic of The Gambia
	/// 
	/// alpha3: GMB
	/// 
	/// alpha2: GM
	/// 
	/// numeric: 270
	static const GMB = IsoCountry._(Countries.gmb);

	/// The Republic of Guinea-Bissau
	/// 
	/// alpha3: GNB
	/// 
	/// alpha2: GW
	/// 
	/// numeric: 624
	static const GNB = IsoCountry._(Countries.gnb);

	/// The Republic of Equatorial Guinea
	/// 
	/// alpha3: GNQ
	/// 
	/// alpha2: GQ
	/// 
	/// numeric: 226
	static const GNQ = IsoCountry._(Countries.gnq);

	/// The Hellenic Republic
	/// 
	/// alpha3: GRC
	/// 
	/// alpha2: GR
	/// 
	/// numeric: 300
	static const GRC = IsoCountry._(Countries.grc);

	/// Grenada
	/// 
	/// alpha3: GRD
	/// 
	/// alpha2: GD
	/// 
	/// numeric: 308
	static const GRD = IsoCountry._(Countries.grd);

	/// Kalaallit Nunaat
	/// 
	/// alpha3: GRL
	/// 
	/// alpha2: GL
	/// 
	/// numeric: 304
	static const GRL = IsoCountry._(Countries.grl);

	/// The Republic of Guatemala
	/// 
	/// alpha3: GTM
	/// 
	/// alpha2: GT
	/// 
	/// numeric: 320
	static const GTM = IsoCountry._(Countries.gtm);

	/// Guyane
	/// 
	/// alpha3: GUF
	/// 
	/// alpha2: GF
	/// 
	/// numeric: 254
	static const GUF = IsoCountry._(Countries.guf);

	/// The Territory of Guam
	/// 
	/// alpha3: GUM
	/// 
	/// alpha2: GU
	/// 
	/// numeric: 316
	static const GUM = IsoCountry._(Countries.gum);

	/// The Co-operative Republic of Guyana
	/// 
	/// alpha3: GUY
	/// 
	/// alpha2: GY
	/// 
	/// numeric: 328
	static const GUY = IsoCountry._(Countries.guy);

	/// The Hong Kong Special Administrative Region of China
	/// 
	/// alpha3: HKG
	/// 
	/// alpha2: HK
	/// 
	/// numeric: 344
	static const HKG = IsoCountry._(Countries.hkg);

	/// The Territory of Heard Island and McDonald Islands
	/// 
	/// alpha3: HMD
	/// 
	/// alpha2: HM
	/// 
	/// numeric: 334
	static const HMD = IsoCountry._(Countries.hmd);

	/// The Republic of Honduras
	/// 
	/// alpha3: HND
	/// 
	/// alpha2: HN
	/// 
	/// numeric: 340
	static const HND = IsoCountry._(Countries.hnd);

	/// The Republic of Croatia
	/// 
	/// alpha3: HRV
	/// 
	/// alpha2: HR
	/// 
	/// numeric: 191
	static const HRV = IsoCountry._(Countries.hrv);

	/// The Republic of Haiti
	/// 
	/// alpha3: HTI
	/// 
	/// alpha2: HT
	/// 
	/// numeric: 332
	static const HTI = IsoCountry._(Countries.hti);

	/// Hungary
	/// 
	/// alpha3: HUN
	/// 
	/// alpha2: HU
	/// 
	/// numeric: 348
	static const HUN = IsoCountry._(Countries.hun);

	/// The Republic of Indonesia
	/// 
	/// alpha3: IDN
	/// 
	/// alpha2: ID
	/// 
	/// numeric: 360
	static const IDN = IsoCountry._(Countries.idn);

	/// The Isle of Man
	/// 
	/// alpha3: IMN
	/// 
	/// alpha2: IM
	/// 
	/// numeric: 833
	static const IMN = IsoCountry._(Countries.imn);

	/// The Republic of India
	/// 
	/// alpha3: IND
	/// 
	/// alpha2: IN
	/// 
	/// numeric: 356
	static const IND = IsoCountry._(Countries.ind);

	/// The British Indian Ocean Territory
	/// 
	/// alpha3: IOT
	/// 
	/// alpha2: IO
	/// 
	/// numeric: 086
	static const IOT = IsoCountry._(Countries.iot);

	/// Ireland
	/// 
	/// alpha3: IRL
	/// 
	/// alpha2: IE
	/// 
	/// numeric: 372
	static const IRL = IsoCountry._(Countries.irl);

	/// The Islamic Republic of Iran
	/// 
	/// alpha3: IRN
	/// 
	/// alpha2: IR
	/// 
	/// numeric: 364
	static const IRN = IsoCountry._(Countries.irn);

	/// The Republic of Iraq
	/// 
	/// alpha3: IRQ
	/// 
	/// alpha2: IQ
	/// 
	/// numeric: 368
	static const IRQ = IsoCountry._(Countries.irq);

	/// Iceland
	/// 
	/// alpha3: ISL
	/// 
	/// alpha2: IS
	/// 
	/// numeric: 352
	static const ISL = IsoCountry._(Countries.isl);

	/// The State of Israel
	/// 
	/// alpha3: ISR
	/// 
	/// alpha2: IL
	/// 
	/// numeric: 376
	static const ISR = IsoCountry._(Countries.isr);

	/// The Italian Republic
	/// 
	/// alpha3: ITA
	/// 
	/// alpha2: IT
	/// 
	/// numeric: 380
	static const ITA = IsoCountry._(Countries.ita);

	/// Jamaica
	/// 
	/// alpha3: JAM
	/// 
	/// alpha2: JM
	/// 
	/// numeric: 388
	static const JAM = IsoCountry._(Countries.jam);

	/// The Bailiwick of Jersey
	/// 
	/// alpha3: JEY
	/// 
	/// alpha2: JE
	/// 
	/// numeric: 832
	static const JEY = IsoCountry._(Countries.jey);

	/// The Hashemite Kingdom of Jordan
	/// 
	/// alpha3: JOR
	/// 
	/// alpha2: JO
	/// 
	/// numeric: 400
	static const JOR = IsoCountry._(Countries.jor);

	/// Japan
	/// 
	/// alpha3: JPN
	/// 
	/// alpha2: JP
	/// 
	/// numeric: 392
	static const JPN = IsoCountry._(Countries.jpn);

	/// The Republic of Kazakhstan
	/// 
	/// alpha3: KAZ
	/// 
	/// alpha2: KZ
	/// 
	/// numeric: 398
	static const KAZ = IsoCountry._(Countries.kaz);

	/// The Republic of Kenya
	/// 
	/// alpha3: KEN
	/// 
	/// alpha2: KE
	/// 
	/// numeric: 404
	static const KEN = IsoCountry._(Countries.ken);

	/// The Kyrgyz Republic
	/// 
	/// alpha3: KGZ
	/// 
	/// alpha2: KG
	/// 
	/// numeric: 417
	static const KGZ = IsoCountry._(Countries.kgz);

	/// The Kingdom of Cambodia
	/// 
	/// alpha3: KHM
	/// 
	/// alpha2: KH
	/// 
	/// numeric: 116
	static const KHM = IsoCountry._(Countries.khm);

	/// The Republic of Kiribati
	/// 
	/// alpha3: KIR
	/// 
	/// alpha2: KI
	/// 
	/// numeric: 296
	static const KIR = IsoCountry._(Countries.kir);

	/// Saint Kitts and Nevis
	/// 
	/// alpha3: KNA
	/// 
	/// alpha2: KN
	/// 
	/// numeric: 659
	static const KNA = IsoCountry._(Countries.kna);

	/// The Republic of Korea
	/// 
	/// alpha3: KOR
	/// 
	/// alpha2: KR
	/// 
	/// numeric: 410
	static const KOR = IsoCountry._(Countries.kor);

	/// The State of Kuwait
	/// 
	/// alpha3: KWT
	/// 
	/// alpha2: KW
	/// 
	/// numeric: 414
	static const KWT = IsoCountry._(Countries.kwt);

	/// The Lao People's Democratic Republic
	/// 
	/// alpha3: LAO
	/// 
	/// alpha2: LA
	/// 
	/// numeric: 418
	static const LAO = IsoCountry._(Countries.lao);

	/// The Lebanese Republic
	/// 
	/// alpha3: LBN
	/// 
	/// alpha2: LB
	/// 
	/// numeric: 422
	static const LBN = IsoCountry._(Countries.lbn);

	/// The Republic of Liberia
	/// 
	/// alpha3: LBR
	/// 
	/// alpha2: LR
	/// 
	/// numeric: 430
	static const LBR = IsoCountry._(Countries.lbr);

	/// The State of Libya
	/// 
	/// alpha3: LBY
	/// 
	/// alpha2: LY
	/// 
	/// numeric: 434
	static const LBY = IsoCountry._(Countries.lby);

	/// Saint Lucia
	/// 
	/// alpha3: LCA
	/// 
	/// alpha2: LC
	/// 
	/// numeric: 662
	static const LCA = IsoCountry._(Countries.lca);

	/// The Principality of Liechtenstein
	/// 
	/// alpha3: LIE
	/// 
	/// alpha2: LI
	/// 
	/// numeric: 438
	static const LIE = IsoCountry._(Countries.lie);

	/// The Democratic Socialist Republic of Sri Lanka
	/// 
	/// alpha3: LKA
	/// 
	/// alpha2: LK
	/// 
	/// numeric: 144
	static const LKA = IsoCountry._(Countries.lka);

	/// The Kingdom of Lesotho
	/// 
	/// alpha3: LSO
	/// 
	/// alpha2: LS
	/// 
	/// numeric: 426
	static const LSO = IsoCountry._(Countries.lso);

	/// The Republic of Lithuania
	/// 
	/// alpha3: LTU
	/// 
	/// alpha2: LT
	/// 
	/// numeric: 440
	static const LTU = IsoCountry._(Countries.ltu);

	/// The Grand Duchy of Luxembourg
	/// 
	/// alpha3: LUX
	/// 
	/// alpha2: LU
	/// 
	/// numeric: 442
	static const LUX = IsoCountry._(Countries.lux);

	/// The Republic of Latvia
	/// 
	/// alpha3: LVA
	/// 
	/// alpha2: LV
	/// 
	/// numeric: 428
	static const LVA = IsoCountry._(Countries.lva);

	/// The Macao Special Administrative Region of China
	/// 
	/// alpha3: MAC
	/// 
	/// alpha2: MO
	/// 
	/// numeric: 446
	static const MAC = IsoCountry._(Countries.mac);

	/// The Collectivity of Saint-Martin
	/// 
	/// alpha3: MAF
	/// 
	/// alpha2: MF
	/// 
	/// numeric: 663
	static const MAF = IsoCountry._(Countries.maf);

	/// The Kingdom of Morocco
	/// 
	/// alpha3: MAR
	/// 
	/// alpha2: MA
	/// 
	/// numeric: 504
	static const MAR = IsoCountry._(Countries.mar);

	/// The Principality of Monaco
	/// 
	/// alpha3: MCO
	/// 
	/// alpha2: MC
	/// 
	/// numeric: 492
	static const MCO = IsoCountry._(Countries.mco);

	/// The Republic of Moldova
	/// 
	/// alpha3: MDA
	/// 
	/// alpha2: MD
	/// 
	/// numeric: 498
	static const MDA = IsoCountry._(Countries.mda);

	/// The Republic of Madagascar
	/// 
	/// alpha3: MDG
	/// 
	/// alpha2: MG
	/// 
	/// numeric: 450
	static const MDG = IsoCountry._(Countries.mdg);

	/// The Republic of Maldives
	/// 
	/// alpha3: MDV
	/// 
	/// alpha2: MV
	/// 
	/// numeric: 462
	static const MDV = IsoCountry._(Countries.mdv);

	/// The United Mexican States
	/// 
	/// alpha3: MEX
	/// 
	/// alpha2: MX
	/// 
	/// numeric: 484
	static const MEX = IsoCountry._(Countries.mex);

	/// The Republic of the Marshall Islands
	/// 
	/// alpha3: MHL
	/// 
	/// alpha2: MH
	/// 
	/// numeric: 584
	static const MHL = IsoCountry._(Countries.mhl);

	/// The Republic of North Macedonia
	/// 
	/// alpha3: MKD
	/// 
	/// alpha2: MK
	/// 
	/// numeric: 807
	static const MKD = IsoCountry._(Countries.mkd);

	/// The Republic of Mali
	/// 
	/// alpha3: MLI
	/// 
	/// alpha2: ML
	/// 
	/// numeric: 466
	static const MLI = IsoCountry._(Countries.mli);

	/// The Republic of Malta
	/// 
	/// alpha3: MLT
	/// 
	/// alpha2: MT
	/// 
	/// numeric: 470
	static const MLT = IsoCountry._(Countries.mlt);

	/// The Republic of the Union of Myanmar
	/// 
	/// alpha3: MMR
	/// 
	/// alpha2: MM
	/// 
	/// numeric: 104
	static const MMR = IsoCountry._(Countries.mmr);

	/// Montenegro
	/// 
	/// alpha3: MNE
	/// 
	/// alpha2: ME
	/// 
	/// numeric: 499
	static const MNE = IsoCountry._(Countries.mne);

	/// Mongolia
	/// 
	/// alpha3: MNG
	/// 
	/// alpha2: MN
	/// 
	/// numeric: 496
	static const MNG = IsoCountry._(Countries.mng);

	/// The Commonwealth of the Northern Mariana Islands
	/// 
	/// alpha3: MNP
	/// 
	/// alpha2: MP
	/// 
	/// numeric: 580
	static const MNP = IsoCountry._(Countries.mnp);

	/// The Republic of Mozambique
	/// 
	/// alpha3: MOZ
	/// 
	/// alpha2: MZ
	/// 
	/// numeric: 508
	static const MOZ = IsoCountry._(Countries.moz);

	/// The Islamic Republic of Mauritania
	/// 
	/// alpha3: MRT
	/// 
	/// alpha2: MR
	/// 
	/// numeric: 478
	static const MRT = IsoCountry._(Countries.mrt);

	/// Montserrat
	/// 
	/// alpha3: MSR
	/// 
	/// alpha2: MS
	/// 
	/// numeric: 500
	static const MSR = IsoCountry._(Countries.msr);

	/// Martinique
	/// 
	/// alpha3: MTQ
	/// 
	/// alpha2: MQ
	/// 
	/// numeric: 474
	static const MTQ = IsoCountry._(Countries.mtq);

	/// The Republic of Mauritius
	/// 
	/// alpha3: MUS
	/// 
	/// alpha2: MU
	/// 
	/// numeric: 480
	static const MUS = IsoCountry._(Countries.mus);

	/// The Republic of Malawi
	/// 
	/// alpha3: MWI
	/// 
	/// alpha2: MW
	/// 
	/// numeric: 454
	static const MWI = IsoCountry._(Countries.mwi);

	/// Malaysia
	/// 
	/// alpha3: MYS
	/// 
	/// alpha2: MY
	/// 
	/// numeric: 458
	static const MYS = IsoCountry._(Countries.mys);

	/// The Department of Mayotte
	/// 
	/// alpha3: MYT
	/// 
	/// alpha2: YT
	/// 
	/// numeric: 175
	static const MYT = IsoCountry._(Countries.myt);

	/// The Republic of Namibia
	/// 
	/// alpha3: NAM
	/// 
	/// alpha2: NA
	/// 
	/// numeric: 516
	static const NAM = IsoCountry._(Countries.nam);

	/// New Caledonia
	/// 
	/// alpha3: NCL
	/// 
	/// alpha2: NC
	/// 
	/// numeric: 540
	static const NCL = IsoCountry._(Countries.ncl);

	/// The Republic of the Niger
	/// 
	/// alpha3: NER
	/// 
	/// alpha2: NE
	/// 
	/// numeric: 562
	static const NER = IsoCountry._(Countries.ner);

	/// The Territory of Norfolk Island
	/// 
	/// alpha3: NFK
	/// 
	/// alpha2: NF
	/// 
	/// numeric: 574
	static const NFK = IsoCountry._(Countries.nfk);

	/// The Federal Republic of Nigeria
	/// 
	/// alpha3: NGA
	/// 
	/// alpha2: NG
	/// 
	/// numeric: 566
	static const NGA = IsoCountry._(Countries.nga);

	/// The Republic of Nicaragua
	/// 
	/// alpha3: NIC
	/// 
	/// alpha2: NI
	/// 
	/// numeric: 558
	static const NIC = IsoCountry._(Countries.nic);

	/// Niue
	/// 
	/// alpha3: NIU
	/// 
	/// alpha2: NU
	/// 
	/// numeric: 570
	static const NIU = IsoCountry._(Countries.niu);

	/// The Kingdom of the Netherlands
	/// 
	/// alpha3: NLD
	/// 
	/// alpha2: NL
	/// 
	/// numeric: 528
	static const NLD = IsoCountry._(Countries.nld);

	/// The Kingdom of Norway
	/// 
	/// alpha3: NOR
	/// 
	/// alpha2: NO
	/// 
	/// numeric: 578
	static const NOR = IsoCountry._(Countries.nor);

	/// The Federal Democratic Republic of Nepal
	/// 
	/// alpha3: NPL
	/// 
	/// alpha2: NP
	/// 
	/// numeric: 524
	static const NPL = IsoCountry._(Countries.npl);

	/// The Republic of Nauru
	/// 
	/// alpha3: NRU
	/// 
	/// alpha2: NR
	/// 
	/// numeric: 520
	static const NRU = IsoCountry._(Countries.nru);

	/// New Zealand
	/// 
	/// alpha3: NZL
	/// 
	/// alpha2: NZ
	/// 
	/// numeric: 554
	static const NZL = IsoCountry._(Countries.nzl);

	/// The Sultanate of Oman
	/// 
	/// alpha3: OMN
	/// 
	/// alpha2: OM
	/// 
	/// numeric: 512
	static const OMN = IsoCountry._(Countries.omn);

	/// The Islamic Republic of Pakistan
	/// 
	/// alpha3: PAK
	/// 
	/// alpha2: PK
	/// 
	/// numeric: 586
	static const PAK = IsoCountry._(Countries.pak);

	/// The Republic of Panamá
	/// 
	/// alpha3: PAN
	/// 
	/// alpha2: PA
	/// 
	/// numeric: 591
	static const PAN = IsoCountry._(Countries.pan);

	/// The Pitcairn, Henderson, Ducie and Oeno Islands
	/// 
	/// alpha3: PCN
	/// 
	/// alpha2: PN
	/// 
	/// numeric: 612
	static const PCN = IsoCountry._(Countries.pcn);

	/// The Republic of Perú
	/// 
	/// alpha3: PER
	/// 
	/// alpha2: PE
	/// 
	/// numeric: 604
	static const PER = IsoCountry._(Countries.per);

	/// The Republic of the Philippines
	/// 
	/// alpha3: PHL
	/// 
	/// alpha2: PH
	/// 
	/// numeric: 608
	static const PHL = IsoCountry._(Countries.phl);

	/// The Republic of Palau
	/// 
	/// alpha3: PLW
	/// 
	/// alpha2: PW
	/// 
	/// numeric: 585
	static const PLW = IsoCountry._(Countries.plw);

	/// The Independent State of Papua New Guinea
	/// 
	/// alpha3: PNG
	/// 
	/// alpha2: PG
	/// 
	/// numeric: 598
	static const PNG = IsoCountry._(Countries.png);

	/// The Republic of Poland
	/// 
	/// alpha3: POL
	/// 
	/// alpha2: PL
	/// 
	/// numeric: 616
	static const POL = IsoCountry._(Countries.pol);

	/// The Commonwealth of Puerto Rico
	/// 
	/// alpha3: PRI
	/// 
	/// alpha2: PR
	/// 
	/// numeric: 630
	static const PRI = IsoCountry._(Countries.pri);

	/// The Democratic People's Republic of Korea
	/// 
	/// alpha3: PRK
	/// 
	/// alpha2: KP
	/// 
	/// numeric: 408
	static const PRK = IsoCountry._(Countries.prk);

	/// The Portuguese Republic
	/// 
	/// alpha3: PRT
	/// 
	/// alpha2: PT
	/// 
	/// numeric: 620
	static const PRT = IsoCountry._(Countries.prt);

	/// The Republic of Paraguay
	/// 
	/// alpha3: PRY
	/// 
	/// alpha2: PY
	/// 
	/// numeric: 600
	static const PRY = IsoCountry._(Countries.pry);

	/// The State of Palestine
	/// 
	/// alpha3: PSE
	/// 
	/// alpha2: PS
	/// 
	/// numeric: 275
	static const PSE = IsoCountry._(Countries.pse);

	/// French Polynesia
	/// 
	/// alpha3: PYF
	/// 
	/// alpha2: PF
	/// 
	/// numeric: 258
	static const PYF = IsoCountry._(Countries.pyf);

	/// The State of Qatar
	/// 
	/// alpha3: QAT
	/// 
	/// alpha2: QA
	/// 
	/// numeric: 634
	static const QAT = IsoCountry._(Countries.qat);

	/// Réunion
	/// 
	/// alpha3: REU
	/// 
	/// alpha2: RE
	/// 
	/// numeric: 638
	static const REU = IsoCountry._(Countries.reu);

	/// Romania
	/// 
	/// alpha3: ROU
	/// 
	/// alpha2: RO
	/// 
	/// numeric: 642
	static const ROU = IsoCountry._(Countries.rou);

	/// The Russian Federation
	/// 
	/// alpha3: RUS
	/// 
	/// alpha2: RU
	/// 
	/// numeric: 643
	static const RUS = IsoCountry._(Countries.rus);

	/// The Republic of Rwanda
	/// 
	/// alpha3: RWA
	/// 
	/// alpha2: RW
	/// 
	/// numeric: 646
	static const RWA = IsoCountry._(Countries.rwa);

	/// The Kingdom of Saudi Arabia
	/// 
	/// alpha3: SAU
	/// 
	/// alpha2: SA
	/// 
	/// numeric: 682
	static const SAU = IsoCountry._(Countries.sau);

	/// The Republic of the Sudan
	/// 
	/// alpha3: SDN
	/// 
	/// alpha2: SD
	/// 
	/// numeric: 729
	static const SDN = IsoCountry._(Countries.sdn);

	/// The Republic of Senegal
	/// 
	/// alpha3: SEN
	/// 
	/// alpha2: SN
	/// 
	/// numeric: 686
	static const SEN = IsoCountry._(Countries.sen);

	/// The Republic of Singapore
	/// 
	/// alpha3: SGP
	/// 
	/// alpha2: SG
	/// 
	/// numeric: 702
	static const SGP = IsoCountry._(Countries.sgp);

	/// South Georgia and the South Sandwich Islands
	/// 
	/// alpha3: SGS
	/// 
	/// alpha2: GS
	/// 
	/// numeric: 239
	static const SGS = IsoCountry._(Countries.sgs);

	/// Saint Helena, Ascension and Tristan da Cunha
	/// 
	/// alpha3: SHN
	/// 
	/// alpha2: SH
	/// 
	/// numeric: 654
	static const SHN = IsoCountry._(Countries.shn);

	/// Svalbard and Jan Mayen
	/// 
	/// alpha3: SJM
	/// 
	/// alpha2: SJ
	/// 
	/// numeric: 744
	static const SJM = IsoCountry._(Countries.sjm);

	/// The Solomon Islands
	/// 
	/// alpha3: SLB
	/// 
	/// alpha2: SB
	/// 
	/// numeric: 090
	static const SLB = IsoCountry._(Countries.slb);

	/// The Republic of Sierra Leone
	/// 
	/// alpha3: SLE
	/// 
	/// alpha2: SL
	/// 
	/// numeric: 694
	static const SLE = IsoCountry._(Countries.sle);

	/// The Republic of El Salvador
	/// 
	/// alpha3: SLV
	/// 
	/// alpha2: SV
	/// 
	/// numeric: 222
	static const SLV = IsoCountry._(Countries.slv);

	/// The Republic of San Marino
	/// 
	/// alpha3: SMR
	/// 
	/// alpha2: SM
	/// 
	/// numeric: 674
	static const SMR = IsoCountry._(Countries.smr);

	/// The Federal Republic of Somalia
	/// 
	/// alpha3: SOM
	/// 
	/// alpha2: SO
	/// 
	/// numeric: 706
	static const SOM = IsoCountry._(Countries.som);

	/// The Overseas Collectivity of Saint-Pierre and Miquelon
	/// 
	/// alpha3: SPM
	/// 
	/// alpha2: PM
	/// 
	/// numeric: 666
	static const SPM = IsoCountry._(Countries.spm);

	/// The Republic of Serbia
	/// 
	/// alpha3: SRB
	/// 
	/// alpha2: RS
	/// 
	/// numeric: 688
	static const SRB = IsoCountry._(Countries.srb);

	/// The Republic of South Sudan
	/// 
	/// alpha3: SSD
	/// 
	/// alpha2: SS
	/// 
	/// numeric: 728
	static const SSD = IsoCountry._(Countries.ssd);

	/// The Democratic Republic of São Tomé and Príncipe
	/// 
	/// alpha3: STP
	/// 
	/// alpha2: ST
	/// 
	/// numeric: 678
	static const STP = IsoCountry._(Countries.stp);

	/// The Republic of Suriname
	/// 
	/// alpha3: SUR
	/// 
	/// alpha2: SR
	/// 
	/// numeric: 740
	static const SUR = IsoCountry._(Countries.sur);

	/// The Slovak Republic
	/// 
	/// alpha3: SVK
	/// 
	/// alpha2: SK
	/// 
	/// numeric: 703
	static const SVK = IsoCountry._(Countries.svk);

	/// The Republic of Slovenia
	/// 
	/// alpha3: SVN
	/// 
	/// alpha2: SI
	/// 
	/// numeric: 705
	static const SVN = IsoCountry._(Countries.svn);

	/// The Kingdom of Sweden
	/// 
	/// alpha3: SWE
	/// 
	/// alpha2: SE
	/// 
	/// numeric: 752
	static const SWE = IsoCountry._(Countries.swe);

	/// The Kingdom of Eswatini
	/// 
	/// alpha3: SWZ
	/// 
	/// alpha2: SZ
	/// 
	/// numeric: 748
	static const SWZ = IsoCountry._(Countries.swz);

	/// Sint Maarten
	/// 
	/// alpha3: SXM
	/// 
	/// alpha2: SX
	/// 
	/// numeric: 534
	static const SXM = IsoCountry._(Countries.sxm);

	/// The Republic of Seychelles
	/// 
	/// alpha3: SYC
	/// 
	/// alpha2: SC
	/// 
	/// numeric: 690
	static const SYC = IsoCountry._(Countries.syc);

	/// The Syrian Arab Republic
	/// 
	/// alpha3: SYR
	/// 
	/// alpha2: SY
	/// 
	/// numeric: 760
	static const SYR = IsoCountry._(Countries.syr);

	/// The Turks and Caicos Islands
	/// 
	/// alpha3: TCA
	/// 
	/// alpha2: TC
	/// 
	/// numeric: 796
	static const TCA = IsoCountry._(Countries.tca);

	/// The Republic of Chad
	/// 
	/// alpha3: TCD
	/// 
	/// alpha2: TD
	/// 
	/// numeric: 148
	static const TCD = IsoCountry._(Countries.tcd);

	/// The Togolese Republic
	/// 
	/// alpha3: TGO
	/// 
	/// alpha2: TG
	/// 
	/// numeric: 768
	static const TGO = IsoCountry._(Countries.tgo);

	/// The Kingdom of Thailand
	/// 
	/// alpha3: THA
	/// 
	/// alpha2: TH
	/// 
	/// numeric: 764
	static const THA = IsoCountry._(Countries.tha);

	/// The Republic of Tajikistan
	/// 
	/// alpha3: TJK
	/// 
	/// alpha2: TJ
	/// 
	/// numeric: 762
	static const TJK = IsoCountry._(Countries.tjk);

	/// Tokelau
	/// 
	/// alpha3: TKL
	/// 
	/// alpha2: TK
	/// 
	/// numeric: 772
	static const TKL = IsoCountry._(Countries.tkl);

	/// Turkmenistan
	/// 
	/// alpha3: TKM
	/// 
	/// alpha2: TM
	/// 
	/// numeric: 795
	static const TKM = IsoCountry._(Countries.tkm);

	/// The Democratic Republic of Timor-Leste
	/// 
	/// alpha3: TLS
	/// 
	/// alpha2: TL
	/// 
	/// numeric: 626
	static const TLS = IsoCountry._(Countries.tls);

	/// The Kingdom of Tonga
	/// 
	/// alpha3: TON
	/// 
	/// alpha2: TO
	/// 
	/// numeric: 776
	static const TON = IsoCountry._(Countries.ton);

	/// The Republic of Trinidad and Tobago
	/// 
	/// alpha3: TTO
	/// 
	/// alpha2: TT
	/// 
	/// numeric: 780
	static const TTO = IsoCountry._(Countries.tto);

	/// The Republic of Tunisia
	/// 
	/// alpha3: TUN
	/// 
	/// alpha2: TN
	/// 
	/// numeric: 788
	static const TUN = IsoCountry._(Countries.tun);

	/// The Republic of Türkiye
	/// 
	/// alpha3: TUR
	/// 
	/// alpha2: TR
	/// 
	/// numeric: 792
	static const TUR = IsoCountry._(Countries.tur);

	/// Tuvalu
	/// 
	/// alpha3: TUV
	/// 
	/// alpha2: TV
	/// 
	/// numeric: 798
	static const TUV = IsoCountry._(Countries.tuv);

	/// Taiwan, Province of China
	/// 
	/// alpha3: TWN
	/// 
	/// alpha2: TW
	/// 
	/// numeric: 158
	static const TWN = IsoCountry._(Countries.twn);

	/// The United Republic of Tanzania
	/// 
	/// alpha3: TZA
	/// 
	/// alpha2: TZ
	/// 
	/// numeric: 834
	static const TZA = IsoCountry._(Countries.tza);

	/// The Republic of Uganda
	/// 
	/// alpha3: UGA
	/// 
	/// alpha2: UG
	/// 
	/// numeric: 800
	static const UGA = IsoCountry._(Countries.uga);

	/// Ukraine
	/// 
	/// alpha3: UKR
	/// 
	/// alpha2: UA
	/// 
	/// numeric: 804
	static const UKR = IsoCountry._(Countries.ukr);

	/// United States Minor Outlying Islands
	/// 
	/// alpha3: UMI
	/// 
	/// alpha2: UM
	/// 
	/// numeric: 581
	static const UMI = IsoCountry._(Countries.umi);

	/// The Oriental Republic of Uruguay
	/// 
	/// alpha3: URY
	/// 
	/// alpha2: UY
	/// 
	/// numeric: 858
	static const URY = IsoCountry._(Countries.ury);

	/// The United States of America
	/// 
	/// alpha3: USA
	/// 
	/// alpha2: US
	/// 
	/// numeric: 840
	static const USA = IsoCountry._(Countries.usa);

	/// The Republic of Uzbekistan
	/// 
	/// alpha3: UZB
	/// 
	/// alpha2: UZ
	/// 
	/// numeric: 860
	static const UZB = IsoCountry._(Countries.uzb);

	/// The Holy See
	/// 
	/// alpha3: VAT
	/// 
	/// alpha2: VA
	/// 
	/// numeric: 336
	static const VAT = IsoCountry._(Countries.vat);

	/// Saint Vincent and the Grenadines
	/// 
	/// alpha3: VCT
	/// 
	/// alpha2: VC
	/// 
	/// numeric: 670
	static const VCT = IsoCountry._(Countries.vct);

	/// The Bolivarian Republic of Venezuela
	/// 
	/// alpha3: VEN
	/// 
	/// alpha2: VE
	/// 
	/// numeric: 862
	static const VEN = IsoCountry._(Countries.ven);

	/// The Virgin Islands
	/// 
	/// alpha3: VGB
	/// 
	/// alpha2: VG
	/// 
	/// numeric: 092
	static const VGB = IsoCountry._(Countries.vgb);

	/// The Virgin Islands of the United States
	/// 
	/// alpha3: VIR
	/// 
	/// alpha2: VI
	/// 
	/// numeric: 850
	static const VIR = IsoCountry._(Countries.vir);

	/// The Socialist Republic of Viet Nam
	/// 
	/// alpha3: VNM
	/// 
	/// alpha2: VN
	/// 
	/// numeric: 704
	static const VNM = IsoCountry._(Countries.vnm);

	/// The Republic of Vanuatu
	/// 
	/// alpha3: VUT
	/// 
	/// alpha2: VU
	/// 
	/// numeric: 548
	static const VUT = IsoCountry._(Countries.vut);

	/// The Territory of the Wallis and Futuna Islands
	/// 
	/// alpha3: WLF
	/// 
	/// alpha2: WF
	/// 
	/// numeric: 876
	static const WLF = IsoCountry._(Countries.wlf);

	/// The Independent State of Samoa
	/// 
	/// alpha3: WSM
	/// 
	/// alpha2: WS
	/// 
	/// numeric: 882
	static const WSM = IsoCountry._(Countries.wsm);

	/// The Republic of Kosovo
	/// 
	/// alpha3: XXK
	/// 
	/// alpha2: XK
	/// 
	/// numeric: 900
	static const XXK = IsoCountry._(LibraryCountries.XXK);

	/// The Republic of Yemen
	/// 
	/// alpha3: YEM
	/// 
	/// alpha2: YE
	/// 
	/// numeric: 887
	static const YEM = IsoCountry._(Countries.yem);

	/// The Republic of South Africa
	/// 
	/// alpha3: ZAF
	/// 
	/// alpha2: ZA
	/// 
	/// numeric: 710
	static const ZAF = IsoCountry._(Countries.zaf);

	/// The Republic of Zambia
	/// 
	/// alpha3: ZMB
	/// 
	/// alpha2: ZM
	/// 
	/// numeric: 894
	static const ZMB = IsoCountry._(Countries.zmb);

	/// The Republic of Zimbabwe
	/// 
	/// alpha3: ZWE
	/// 
	/// alpha2: ZW
	/// 
	/// numeric: 716
	static const ZWE = IsoCountry._(Countries.zwe);

	static const values = [
		ABW,
		AFG,
		AGO,
		AIA,
		ALA,
		ALB,
		AND,
		ARE,
		ARG,
		ARM,
		ASM,
		ATA,
		ATF,
		ATG,
		AUS,
		AUT,
		AZE,
		BDI,
		BEL,
		BEN,
		BES,
		BFA,
		BGD,
		BGR,
		BHR,
		BHS,
		BIH,
		BLM,
		BLR,
		BLZ,
		BMU,
		BOL,
		BRA,
		BRB,
		BRN,
		BTN,
		BVT,
		BWA,
		CAF,
		CAN,
		CCK,
		CHE,
		CHL,
		CHN,
		CIV,
		CMR,
		COD,
		COG,
		COK,
		COL,
		COM,
		CPV,
		CRI,
		CUB,
		CUW,
		CXR,
		CYM,
		CYP,
		CZE,
		DEU,
		DJI,
		DMA,
		DNK,
		DOM,
		DZA,
		ECU,
		EGY,
		ERI,
		ESH,
		ESP,
		EST,
		ETH,
		FIN,
		FJI,
		FLK,
		FRA,
		FRO,
		FSM,
		GAB,
		GBR,
		GEO,
		GGY,
		GHA,
		GIB,
		GIN,
		GLP,
		GMB,
		GNB,
		GNQ,
		GRC,
		GRD,
		GRL,
		GTM,
		GUF,
		GUM,
		GUY,
		HKG,
		HMD,
		HND,
		HRV,
		HTI,
		HUN,
		IDN,
		IMN,
		IND,
		IOT,
		IRL,
		IRN,
		IRQ,
		ISL,
		ISR,
		ITA,
		JAM,
		JEY,
		JOR,
		JPN,
		KAZ,
		KEN,
		KGZ,
		KHM,
		KIR,
		KNA,
		KOR,
		KWT,
		LAO,
		LBN,
		LBR,
		LBY,
		LCA,
		LIE,
		LKA,
		LSO,
		LTU,
		LUX,
		LVA,
		MAC,
		MAF,
		MAR,
		MCO,
		MDA,
		MDG,
		MDV,
		MEX,
		MHL,
		MKD,
		MLI,
		MLT,
		MMR,
		MNE,
		MNG,
		MNP,
		MOZ,
		MRT,
		MSR,
		MTQ,
		MUS,
		MWI,
		MYS,
		MYT,
		NAM,
		NCL,
		NER,
		NFK,
		NGA,
		NIC,
		NIU,
		NLD,
		NOR,
		NPL,
		NRU,
		NZL,
		OMN,
		PAK,
		PAN,
		PCN,
		PER,
		PHL,
		PLW,
		PNG,
		POL,
		PRI,
		PRK,
		PRT,
		PRY,
		PSE,
		PYF,
		QAT,
		REU,
		ROU,
		RUS,
		RWA,
		SAU,
		SDN,
		SEN,
		SGP,
		SGS,
		SHN,
		SJM,
		SLB,
		SLE,
		SLV,
		SMR,
		SOM,
		SPM,
		SRB,
		SSD,
		STP,
		SUR,
		SVK,
		SVN,
		SWE,
		SWZ,
		SXM,
		SYC,
		SYR,
		TCA,
		TCD,
		TGO,
		THA,
		TJK,
		TKL,
		TKM,
		TLS,
		TON,
		TTO,
		TUN,
		TUR,
		TUV,
		TWN,
		TZA,
		UGA,
		UKR,
		UMI,
		URY,
		USA,
		UZB,
		VAT,
		VCT,
		VEN,
		VGB,
		VIR,
		VNM,
		VUT,
		WLF,
		WSM,
		XXK,
		YEM,
		ZAF,
		ZMB,
		ZWE,
];
}
