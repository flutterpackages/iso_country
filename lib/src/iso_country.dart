import "dart:collection";

import "package:country/country.dart";
import "package:dartx/dartx.dart";
import "package:meta/meta.dart";

part "iso_country.g.dart";
part "library_countries.dart";
part "utils.dart";

@immutable
class IsoCountry {
  const IsoCountry._(this.data);

  final Country data;

  /// Alpha-2 code as defined in (ISO 3166-1)[https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2]
  String get alpha2 => data.alpha2;

  /// Alpha-3 code as defined in (ISO 3166-1)[https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3]
  String get alpha3 => data.alpha3;

  /// Numeric code as defined in (ISO 3166-1)[https://en.wikipedia.org/wiki/ISO_3166-1_numeric]
  String get numeric => data.number;

  /// Country short name which is defined by ISO 3166-1, a standard defining
  /// codes for the names of countries, dependent territories, and special
  /// areas of geographical interest.
  String get isoShortName => data.isoShortName;

  /// Map for storing country short name by locale
  ///
  /// See [IANA Language Subtag Registry](https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry).
  Map<String, String> get isoShortNameByLanguageCode =>
      data.isoShortNameByLocale.asUnmodifiable();

  /// An official language is a language given a special status in a particular
  /// country, state, or other jurisdiction.
  ///
  /// Typically the term "official language" does not refer to the language used
  /// by a people or country, but by its government
  /// (e.g. judiciary, legislature, and/or administration).
  List<String> get languagesOfficial => data.languagesOfficial.asUnmodifiable();

  /// This list shows countries/disputed countries organized by the languages
  /// which are spoken there.
  List<String> get languagesSpoken => data.languagesSpoken.asUnmodifiable();

  /// Emoji flags are supported on all major platforms except Windows,
  /// which displays two-letter country codes instead of emoji flag images.
  String get flagEmoji => data.flagEmoji;

  @override
  int get hashCode => alpha3.hashCode;

  @override
  bool operator ==(Object other) =>
      other is IsoCountry && other.hashCode == hashCode;

  /// Parses [source] as Alpha-2, alpha-3 or numeric country code
  /// The [source] must be either 2-3 ASCII uppercase letters of alpha code,
  /// or 2-3 digits of numeric code
  /// Throws `FormatException` if the code is not valid country code
  static IsoCountry parse(String source) {
    final country = tryParse(source);
    if (country == null)
      throw FormatException("Invalid or non-assigned code", source);

    return country;
  }

  /// Parses [source] as Alpha-2, alpha-3 or numeric country code.
  /// Same as [parse] but returns `null` in case of invalid country code
  static IsoCountry? tryParse(String source) {
    final numeric = int.tryParse(source)?.toString().padLeft(3, "0");
    if (null != numeric)
      return IsoCountries.values.firstOrNullWhere((e) => e.numeric == numeric);

    final upperCaseSource = source.toUpperCase();
    switch (upperCaseSource) {
      case String(length: 2):
        return IsoCountries.values
            .firstOrNullWhere((e) => e.alpha2 == upperCaseSource);

      case String(length: 3):
        return IsoCountries.values
            .firstOrNullWhere((e) => e.alpha3 == upperCaseSource);

      default:
        return null;
    }
  }

  @override
  String toString() => "$isoShortName "
      "(alpha3: $alpha3, alpha2: $alpha2, numeric: $numeric)";
}
