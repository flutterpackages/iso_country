part of "iso_country.dart";

extension _UnmodifiableListExtension<T> on List<T> {
  UnmodifiableListView<T> asUnmodifiable() => UnmodifiableListView(this);
}

extension _UnmodifiableMapExtension<K, V> on Map<K, V> {
  UnmodifiableMapView<K, V> asUnmodifiable() => UnmodifiableMapView(this);
}
