import "dart:io";
import "dart:mirrors";

import "package:country/country.dart";
import "package:dartx/dartx_io.dart";
import "package:iso_country/src/iso_country.dart";
import "package:path/path.dart" as path;

Future<void> main() async {
  final scriptFile =
      File(Platform.script.toFilePath(windows: Platform.isWindows));
  final srcDir = Directory(
    path.join(
      scriptFile.parent.parent.path,
      "lib",
      "src",
    ),
  );

  final sink = srcDir.file("iso_country.g.dart").openWrite();

  sink.write("""
// GENERATED CODE - DO NOT MODIFY BY HAND

part of "iso_country.dart";

""");

  final countriesClass = reflectClass(Countries);
  final libraryCountriesClass = reflectClass(LibraryCountries);

  final countryWrappers = [
    ...countriesClass.staticMembers.entries
        .where(
          (e) =>
              e.value.isGetter && Country == e.value.returnType.reflectedType,
        )
        .map((e) => _ReflectedCountryWrapper(countriesClass, e.key)),
    ...libraryCountriesClass.staticMembers.entries
        .map((e) => _ReflectedCountryWrapper(libraryCountriesClass, e.key)),
  ]..sort(
      (a, b) => a.country.alpha3.compareTo(b.country.alpha3),
    );

  // open class
  sink.writeln("sealed class IsoCountries {");

  // add static getters for every country
  for (final w in countryWrappers) {
    sink.write(w.getStaticMember());
  }

  // add static getter for values list
  sink.writeln("\tstatic const values = [");
  for (final w in countryWrappers) {
    sink.writeln("\t\t${w.memberName},");
  }
  sink.writeln("];");

  // close class
  sink.writeln("}");

  await sink.flush();
  await sink.close();
}

extension SymbolNameExtension on Symbol {
  String get name {
    final s = toString();
    return s.substring(s.indexOf('"') + 1, s.lastIndexOf('"'));
  }
}

//

sealed class _CountryWrapper {
  const _CountryWrapper();

  String get _assignment;

  Country get country;

  String get memberName => country.alpha3;

  String getStaticMember({
    String indent = "\t",
    bool appendNewLine = true,
  }) {
    return """
$indent/// ${country.isoLongName}
$indent/// 
$indent/// alpha3: ${country.alpha3}
$indent/// 
$indent/// alpha2: ${country.alpha2}
$indent/// 
$indent/// numeric: ${country.number}
${indent}static const $memberName = IsoCountry._($_assignment);${appendNewLine ? "\n" : ""}
""";
  }
}

class _ReflectedCountryWrapper extends _CountryWrapper {
  const _ReflectedCountryWrapper(this.clazz, this.symbol);

  final ClassMirror clazz;
  final Symbol symbol;

  @override
  Country get country => clazz.getField(symbol).reflectee as Country;

  @override
  String get _assignment => "${clazz.simpleName.name}.${symbol.name}";
}
