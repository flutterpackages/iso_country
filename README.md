Generated ISO country classes for every country from the [ISO 3166 standard](https://en.wikipedia.org/wiki/ISO_3166).

Also includes a user defined `IsoCountry` instance for the Repulic of Kosovo.